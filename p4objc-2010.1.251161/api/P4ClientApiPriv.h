/*******************************************************************************

Copyright (c) 2001-2009, Perforce Software, Inc.  All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL PERFORCE SOFTWARE, INC. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*******************************************************************************/

/*===================================================================\
| Name       : P4ClientApiPriv.h
|
| Author     : Michael Bishop <mbishop@perforce.com>
|
| Description: Objective-C wrapper for the Perforce API. The private
 *             methods to handle raw server output.
\===================================================================*/

#import <Foundation/Foundation.h>
#include "clientapi.h"

@class P4ClientApi;
@protocol P4ClientApiCommandDelegate;

class ClientUserWrapper : public ClientUser
{
public:
    ClientUserWrapper(P4ClientApi *, id<P4ClientApiCommandDelegate> delegate, NSString * inputData = nil);
    virtual ~ClientUserWrapper();
    
    virtual void    HandleError( Error * error ) override;
    virtual void    OutputInfo( char level, const char *data ) override;
    virtual void    OutputBinary( const char *data, int length ) override;
    virtual void    OutputText( const char *data, int length ) override;
    virtual void    OutputStat( StrDict *varList ) override;
    virtual void    Finished() override;
    virtual void	InputData( StrBuf *strbuf, Error *e ) override;

            void    SetInputData ( NSString *inputData);

private:
    NSThread *callbackThread() const;

    NSString                       *mInputData = nil;
    P4ClientApi                    *mApi;
    id<P4ClientApiCommandDelegate> mDelegate;
    
    void inspectDelegateMethods();
    
    // booleans to cache the existence of the delegate methods
    bool mHasHandleError;
    bool mHasOutputInfo;
    bool mHasOutputBinary;
    bool mHasOutputText;
    bool mHasOutputStat;
    bool mHasFinished;
    
    bool mDelegateIsAnNSObject;
};


// Utility methods to convert Perforce objects to Cocoa objects

NSString *     StringFromUtf8Bytes( const char * bytes, int length );
NSString *     StringFromUtf8StrPtr( const StrPtr * str );
NSString *     StringFromUtf8CString( const char * str );
NSDictionary * DictionaryFromStrDict( StrDict * strDict );
NSError *      NSErrorFromError( Error * error );


