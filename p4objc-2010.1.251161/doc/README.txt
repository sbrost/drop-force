To generate documentation for this module, download the tool "doxygen" from
http://www.doxygen.org and use it to open the "Doxyfile" in this directory.

> cd <path to this directory>
> doxygen
