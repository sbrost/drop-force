/*******************************************************************************

Copyright (c) 2001-2009, Perforce Software, Inc.  All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL PERFORCE SOFTWARE, INC. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*******************************************************************************/


                                  P4Scout
                  Copyright 2009, Perforce Software, Inc.

This directory contains the complete source to P4Scout, the Perforce iPhone
application.

To build the source, you'll need:

 1 - A binary distribution of the Perforce C++ API for iPhone.
     You can retrieve this from
     ftp://ftp.perforce.com/<release>/bin.iphone30armv6/p4api.tgz

 2 - The source for P4ObjC, the Objective-C api to the Perforce server,
     which came with this distribution


Building P4Scout

    The included project expects that you have a Source Tree called P4APIDIR
    that points to a distribution of the Perforce C++ API.

    You can set a source tree in the XCode Preferences dialog in the
    "Source Trees" section.


Sample Code Status:

    The code included for P4Scout is not intended to be a tutorial on
    how to write an iPhone application, but rather to demonstrate a working
    application that talks to a Perforce server using the Objective-C Client API.

Please direct inquiries to support@perforce.com
