//
//  MonitorTaskTableViewCell.h
//  p4scout
//
//  Created by Work on 12/15/08.
//  Copyright 2008 Perforce Software, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MonitorTaskTableViewCell : UITableViewCell {
    UILabel * userLabel;
    UILabel * fullNameLabel;
    UILabel * commandLabel;
    UILabel * timeLabel;
}

@property (readonly) IBOutlet UILabel * userLabel;
@property (readonly) IBOutlet UILabel * fullNameLabel;
@property (readonly) IBOutlet UILabel * commandLabel;
@property (readonly) IBOutlet UILabel * timeLabel;

@end
