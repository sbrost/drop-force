//
//  NSMutableArray_QueueAdditions.m
//  p4scout
//
//  Created by Work on 1/27/09.
//  Copyright 2009 Perforce Software, Inc. All rights reserved.
//

#import "NSMutableArrayQueueAdditions.h"


@implementation NSMutableArray (P4QueueAdditions)

-(void)enqueueObject:(id)obj
{
    [self addObject:obj];
}

-(id)dequeueObject;
{
    if ([self count] == 0)
        return nil;
    id obj = [[self objectAtIndex:0] retain];
    [self removeObjectAtIndex:0];
    return [obj autorelease];
}

-(id)firstObject
{
    return [self objectAtIndex:0];
}

@end

@implementation NSMutableArray (P4StackAdditions)

-(void)pushObject:(id)obj
{
    [self insertObject:obj atIndex:0];
}

-(id)popObject;
{
    id obj = [[self objectAtIndex: 0] retain];
    [self removeObjectAtIndex: 0];
    return [obj autorelease];
}

@end
