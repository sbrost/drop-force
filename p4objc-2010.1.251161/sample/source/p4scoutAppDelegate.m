//
//  p4scoutAppDelegate.m
//  p4scout
//
//  Created by Work on 3/13/08.
//  Copyright Perforce Software, Inc. 2008. All rights reserved.
//

#import "p4scoutAppDelegate.h"
#import "P4Server.h"
#import "ServerTableController.h"
#import "AboutBoxViewController.h"
#import "P4SettingsViewController.h"
#import "P4DefaultsKeys.h"

#import "FirstRunViewController.h"

@implementation p4scoutAppDelegate

static NSString * const kServerListKey = @"ServerList";

- (void)applicationDidFinishLaunching:(UIApplication *)application
{	
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary * defaultValues = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithBool:YES], kServerDiscoveryPreferenceKey,
                                    [NSNumber numberWithBool:YES], kFirstLaunchPreferenceKey, nil];
    [defaults registerDefaults:defaultValues];
        
    NSData * data = [defaults objectForKey:kServerListKey];

    if ( data )
        serverTableController.servers = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if ( ![serverTableController.servers count] )
    {
        P4Server * s = [[P4Server alloc] init];
        serverTableController.servers = [NSArray arrayWithObject:s];
        [s release];
    }

	// Configure and show the window
	[window addSubview:[navigationController view]];
    [window makeKeyAndVisible];

    BOOL isFirstLaunch = [[NSUserDefaults standardUserDefaults] boolForKey:kFirstLaunchPreferenceKey];

    if ( isFirstLaunch )
        [self showDefaultUserDialog:self];
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	NSData * archive = [NSKeyedArchiver archivedDataWithRootObject:serverTableController.servers];
    [[NSUserDefaults standardUserDefaults] setObject:archive forKey:kServerListKey];

    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kFirstLaunchPreferenceKey];
}

- (void)dealloc {
	[super dealloc];
}


-(IBAction)showDefaultUserDialog:(id)sender
{
    FirstRunViewController * dialog = [[FirstRunViewController alloc] initWithNibName:nil bundle:nil];
    [navigationController presentModalViewController:dialog animated:NO];
}

-(IBAction)showAboutBox:(id)sender
{
    [navigationController presentModalViewController:aboutBoxContainer animated:YES];
}

-(IBAction)showSettings:(id)sender
{
    if ( settingsViewController == nil )
        [[NSBundle mainBundle] loadNibNamed:@"P4SettingsViewController" owner:self options:nil];
    
    [navigationController presentModalViewController:settingsViewController animated:YES];
    [settingsViewController release];
    settingsViewController = nil;
}

-(IBAction)closeAboutBox:(id)sender
{
    [navigationController dismissModalViewControllerAnimated:YES];
}

@end
