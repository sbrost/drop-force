//
//  P4SettingsViewController.h
//  P4Scout
//
//  Created by Michael Bishop on 8/12/09.
//  Copyright 2009 Perforce Software. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface P4SettingsViewController : UINavigationController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {
    IBOutlet UITableViewCell * defaultUserCell;
    IBOutlet UITableViewCell * zeroconfCell;
    IBOutlet UITextField * defaultUserTextField;
    IBOutlet UISwitch * zeroconfSwitch;
    IBOutlet UITableView * tableView;
}

- (IBAction)dismiss:(id)sender;

@end
