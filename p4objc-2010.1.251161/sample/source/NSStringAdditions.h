//
//  NSStringAdditions.h
//  p4scout
//
//  Created by Work on 1/27/09.
//  Copyright 2009 Perforce Software, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSStringAdditions)

+(NSString*)coarseStringWithTimeInterval:(NSTimeInterval)timeInterval;

@end
