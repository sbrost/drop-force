//
//  MonitorTableViewController.m
//  p4scout
//
//  Created by Work on 12/15/08.
//  Copyright 2008 Perforce Software, Inc. All rights reserved.
//

#import "MonitorTableViewController.h"
#import "P4Server.h"
#import "MonitorTaskTableViewCell.h"
#import "NSStringAdditions.h"

@implementation MonitorTableViewController
@synthesize server;

/*
- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if (self = [super initWithStyle:style]) {
    }
    return self;
}
*/

/*
- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
*/

/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

#pragma mark Table view methods

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return server.monitorInfo.count;
}


-(NSString*)fullNameForUserName:(NSString*)username
{
    NSDictionary * userDict = [server.usersInMonitorTable objectForKey:username];
    NSString * fullName = [userDict objectForKey:@"FullName"];
    if ( !fullName )
        return username;
    return fullName;
}

-(NSString*)userDictionary:(NSString*)username
{
    NSDictionary * userDict = [server.usersInMonitorTable objectForKey:username];
    NSString * fullName = [userDict objectForKey:@"FullName"];
    if ( !fullName )
        return username;
    return fullName;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"TaskCell";
    MonitorTaskTableViewCell *cell = (MonitorTaskTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray * a = [[NSBundle mainBundle] loadNibNamed:@"MonitorTaskTableViewCell" owner:self options:nil];
        cell = (MonitorTaskTableViewCell *)[a lastObject];
    }
    
    // Set up the cell...
    NSArray * tasks = server.monitorInfo;
    NSDictionary * d = [tasks objectAtIndex:indexPath.row];
    
//    ... id 9586
//    ... status R
//    ... user mbishopw
//    ... time 00:00:00
//    ... command monitor
    cell.userLabel.text = [d objectForKey:@"user"];
    cell.fullNameLabel.text = [self fullNameForUserName:[d objectForKey:@"user"]];
    cell.commandLabel.text = [d objectForKey:@"command"];
    NSTimeInterval t = TimeIntervalFromString([d objectForKey:@"time"]);
    cell.timeLabel.text = [NSString coarseStringWithTimeInterval:t];
    cell.timeLabel.textColor = [UIColor redColor];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    NSDictionary * task = [server.monitorInfo objectAtIndex:indexPath.row];
    
    if ( !task )
        return;
    
    // This code expects that the email address is valid for that user even though
    // a user can type whatever they want into the email address field
    // of a user spec
    
    NSString * emailAddress = [[server.usersInMonitorTable objectForKey:[task objectForKey:@"user"]] objectForKey:@"Email"];
    
    // catch a blank or non-existent email address
    if ( [emailAddress length] == 0 )
        return;

    //mailto:foo@example.com?cc=bar@example.com&subject=Greetings%20from%20Cupertino!&body=Wish%20you%20were%20here!
    NSString * subject = [NSString stringWithFormat:@"Message about Perforce Task"];
    NSString * body = [NSString stringWithFormat:@"id: %@\nuser: %@\ncommand: %@\ntime: %@",
                       [task objectForKey:@"id"],
                       [task objectForKey:@"user"],
                       [task objectForKey:@"command"],
                       [task objectForKey:@"time"] ];

    if( [MFMailComposeViewController canSendMail] )
    {
        MFMailComposeViewController * mailController = [[[MFMailComposeViewController alloc] init] autorelease];
        [mailController setToRecipients: [NSArray arrayWithObject:emailAddress] ];
        mailController.mailComposeDelegate = self;
        [mailController setSubject:subject];
        [mailController setMessageBody:body isHTML:NO];
        [self presentModalViewController:mailController animated:YES];
        return;
    }
    
    
    NSURL * mailtoURL = [NSURL URLWithString:[[NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@", emailAddress, subject, body]
                                              stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [[UIApplication sharedApplication] openURL:mailtoURL];
}


- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error
{
    [controller dismissModalViewControllerAnimated:YES];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


-(void)observeValueForKeyPath:(NSString *)key
                     ofObject:(id)object
                       change:(NSDictionary *)dict
                      context:(void *)context
{
    if ( ![key isEqualToString:@"monitorInfo"] )
        return;
    [self.tableView reloadData];
}


-(void)setServer:(P4Server *)s
{
    [server removeObserver:self forKeyPath:@"monitorInfo"];

    [s retain];
    [server release];
    server = s;

    [server addObserver:self forKeyPath:@"monitorInfo" options:NSKeyValueObservingOptionNew context:NULL];
    [self.tableView reloadData];
}


- (void)dealloc {
    [server release];
    [super dealloc];
}


@end

