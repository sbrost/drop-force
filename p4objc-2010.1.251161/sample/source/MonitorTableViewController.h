//
//  MonitorTableViewController.h
//  p4scout
//
//  Created by Work on 12/15/08.
//  Copyright 2008 Perforce Software, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class P4Server;
@class MonitorTaskTableViewCell;
#import <MessageUI/MFMailComposeViewController.h>

@interface MonitorTableViewController : UITableViewController <MFMailComposeViewControllerDelegate> {
@private
    P4Server * server;
}

@property (readwrite, retain) P4Server * server;

@end
