//
//  NSMutableArray_QueueAdditions.h
//  p4scout
//
//  Created by Work on 1/27/09.
//  Copyright 2009 Perforce Software, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

// Implements a FIFO Queue

@interface NSMutableArray (P4QueueAdditions)

-(void)enqueueObject:(id)obj; // adds an object to the beginning of the queue
-(id)dequeueObject;           // returns the removed object (which is now autoreleased)
-(id)firstObject;             // returns the first object

@end

@interface NSMutableArray (P4StackAdditions)

-(void)pushObject:(id)obj; // adds an object to the top of the stack
-(id)popObject;            // returns the removed object (which is now autoreleased)

@end
