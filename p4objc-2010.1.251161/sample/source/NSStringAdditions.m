//
//  NSMutableArray_QueueAdditions.m
//  p4scout
//
//  Created by Work on 1/27/09.
//  Copyright 2009 Perforce Software, Inc. All rights reserved.
//

#import "NSStringAdditions.h"


@implementation NSString (NSStringAdditions)

+(NSString*)coarseStringWithTimeInterval:(NSTimeInterval)timeInterval
{
    int seconds = timeInterval;
    if ( seconds < 60 ) // < minute
    {
        return [NSString stringWithFormat:@"%d seconds", seconds];
    }
    else if ( seconds < 60*60 ) // < hour
    {
        return [NSString stringWithFormat:@"%d minutes", (seconds/60)];
    }
    else if ( seconds < 60*60*24 ) // < day
    {
        return [NSString stringWithFormat:@"%d hours", (seconds/(60*60))];
    }

    return [NSString stringWithFormat:@"%d days", (seconds/(60*60*24))];
}

@end
