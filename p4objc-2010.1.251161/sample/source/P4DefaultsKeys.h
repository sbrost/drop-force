/*
 *  P4DefaultsKeys.h
 *  P4Scout
 *
 *  Created by Michael Bishop on 8/17/09.
 *  Copyright 2009 Perforce Software. All rights reserved.
 *
 */


extern NSString * const kFirstLaunchPreferenceKey;
extern NSString * const kDefaultUserPreferenceKey;
extern NSString * const kServerDiscoveryPreferenceKey;
