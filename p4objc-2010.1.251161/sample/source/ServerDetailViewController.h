//
//  ServerDetailViewController.h
//  p4scout
//
//  Created by Work on 10/18/08.
//  Copyright 2008 Perforce Software, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class P4Server;
@class P4ConnectionEditor;
@class MonitorTableViewController;

@interface ServerDetailViewController : UIViewController <UIActionSheetDelegate> {
@private
    IBOutlet P4ConnectionEditor * editViewController;
    IBOutlet UINavigationController * serverEditDialogController;
    IBOutlet MonitorTableViewController * monitorTableViewController;
    IBOutlet UIView   * infoView;
    IBOutlet UIBarButtonItem  * editButton;
    IBOutlet UIBarButtonItem  * sshButton;

    IBOutlet UIActivityIndicatorView   * activityIndicatorView;
    IBOutlet UIToolbar * toolbar;
    IBOutlet UIBarItem * activityIndicatorViewItem;
    
    IBOutlet UITextView  * serverErrorMessage;
    IBOutlet UIImageView  * connectionStatusIcon;
    IBOutlet UILabel  * serverNameLabel;
    IBOutlet UILabel  * serverAddressLabel;
    IBOutlet UILabel  * licenseLabel;
    IBOutlet UILabel  * uptimeLabel;
    IBOutlet UILabel  * uptimeUnitLabel;
    IBOutlet UILabel  * versionLabel;
    IBOutlet UILabel  * platformLabel;
    IBOutlet UILabel  * userCount;
    IBOutlet UILabel  * maxUserCount;
    IBOutlet UITextView  * errorTextView;
    IBOutlet UIProgressView  * userCountProgressView;
             P4Server * server;

    IBOutlet UITextView  * monitorTextView;

    IBOutlet UIImageView  * usersAlertLabel;
    IBOutlet UIImageView  * monitorAlertLabel;
    IBOutlet UIImageView  * licenseAlertLabel;
    IBOutlet UIImageView  * errorAlertLabel;
    IBOutlet UIButton  * deleteButton;
    IBOutlet UIButton  * monitorTableButton;
    IBOutlet UIBarButtonItem  * editBarButtonItem;
    IBOutlet UIImageView  * discoveredBadgeLabel;

    IBOutlet UILabel  * currentCommandLabel;
}

@property (readwrite, retain) P4Server * server;

-(IBAction)edit:(id)sender;
-(IBAction)remove:(id)sender;
-(IBAction)save:(id)sender;
-(IBAction)cancel:(id)sender;

-(IBAction)refresh:(id)sender;
-(IBAction)openSSH:(id)sender;
-(IBAction)showTasks:(id)sender;

@end
