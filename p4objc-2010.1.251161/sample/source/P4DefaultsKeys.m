/*
 *  P4DefaultsKeys.c
 *  P4Scout
 *
 *  Created by Michael Bishop on 8/17/09.
 *  Copyright 2009 Perforce Software. All rights reserved.
 *
 */

#import "P4DefaultsKeys.h"

NSString * const kDefaultUserPreferenceKey  = @"P4USER";
NSString * const kFirstLaunchPreferenceKey  = @"P4FirstLaunch";
NSString * const kServerDiscoveryPreferenceKey  = @"P4UseZeroconf";
