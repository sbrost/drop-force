//
//  SingleServerTableController.h
//  p4scout
//
//  Created by Work on 8/30/08.
//  Copyright 2008 Perforce Software, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class P4Server;

@interface P4ConnectionEditor : UITableViewController <UITextFieldDelegate> {
    P4Server * server;
    BOOL       isNewServer;
    UIView   * fakeFirstResponder; // Really UIKit should allow us access to firstResponder
    UIButton   * deleteButton;
}

@property(readwrite, retain) P4Server * server;
@property (readwrite, assign) BOOL isNewServer;
@property (readwrite, retain) IBOutlet UIButton * deleteButton;

@end
