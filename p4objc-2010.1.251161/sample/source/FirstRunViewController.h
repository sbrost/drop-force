//
//  FirstRunViewController.h
//  P4Scout
//
//  Created by Michael Bishop on 8/7/09.
//  Copyright 2009 Perforce Software. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FirstRunViewController : UIViewController <UITextFieldDelegate> {
@private
    UITextField * defaultUserTextField;
}

@property(retain) IBOutlet UITextField * defaultUserTextField;

-(IBAction)dismiss:(id)sender;

@end
