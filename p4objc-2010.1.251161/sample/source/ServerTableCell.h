//
//  ServerTableCell.h
//  p4scout
//
//  Created by Work on 8/30/08.
//  Copyright 2008 Perforce Software, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ServerTableCell : UITableViewCell {
    UILabel * serverLabel;
    UIActivityIndicatorView * activityView;
}

@property(readwrite, retain) IBOutlet UILabel * serverLabel;
@property(readwrite, retain) IBOutlet UIActivityIndicatorView * activityView;

-(void)dealloc;

@end
