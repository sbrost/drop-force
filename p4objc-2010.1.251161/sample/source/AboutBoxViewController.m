//
//  AboutBoxViewController.m
//  P4Scout
//
//  Created by Michael Bishop on 3/10/09.
//  Copyright 2009 Perforce Software. All rights reserved.
//

#import "AboutBoxViewController.h"


@implementation AboutBoxViewController

/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.rightBarButtonItem = doneButton;

    NSURL * url = [NSURL fileURLWithPath:[[NSBundle mainBundle] resourcePath]];
    url = [NSURL URLWithString:@"index.html" relativeToURL:url];
    [webview loadRequest:[NSURLRequest requestWithURL:url]];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

-(BOOL)webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
navigationType:(UIWebViewNavigationType)navigationType
{
    if ( [[[request URL] scheme] isEqualToString:@"file"] )
        return YES;
    [[UIApplication sharedApplication] openURL:[request URL]];
    return NO;
}


-(IBAction)close:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}


- (void)dealloc {
    [super dealloc];
}


@end
