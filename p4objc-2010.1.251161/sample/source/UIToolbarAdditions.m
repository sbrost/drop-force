//
//  UIToolbarAdditions.h
//  p4scout
//
//  Created by Work on 1/27/09.
//  Copyright 2009 Perforce Software, Inc. All rights reserved.
//

#import "UIToolbarAdditions.h"

@implementation UIToolbar (UIToolbarAdditions)

-(void)insertItem:(UIBarItem *)item inItemsAtIndex:(int)index animated:(BOOL)animated
{
    NSMutableArray * items = [NSMutableArray arrayWithArray:[self items]];
    [items insertObject:item atIndex:index];
    [self setItems:items animated:animated];
}

-(void)removeItemFromItemsAtIndex:(int)index animated:(BOOL)animated
{
    NSMutableArray * items = [NSMutableArray arrayWithArray:[self items]];
    [items removeObjectAtIndex:index];
    [self setItems:items animated:animated];
}

-(void)removeItem:(UIBarItem*)item fromItemsAnimated:(BOOL)animated;
{
    int itemIndex = [[self items] indexOfObject:item ];
    
    if ( itemIndex == NSNotFound )
        return;
    
    [self removeItemFromItemsAtIndex:itemIndex animated:animated];
}

@end
