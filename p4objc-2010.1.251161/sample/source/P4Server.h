//
//  P4Server.h
//  p4scout
//
//  Created by Work on 8/30/08.
//  Copyright 2008 Perforce Software, Inc. All rights reserved.
//

#import <CoreFoundation/CoreFoundation.h>
#import <SystemConfiguration/SystemConfiguration.h>

#import "P4ClientApi.h"


typedef enum ConnectionState {
    Idle = 0,
    Connecting,
    Offline,
    Online
} ConnectionState;

extern NSString * P4ScoutErrorDomain;
extern NSString * P4ServerErrorReceivedNotification;
extern NSString * ActivePropertyKey;
extern NSString * InfoPropertyKey;
extern NSString * NumberOfUsersPropertyKey;
extern NSString * MonitorInfoPropertyKey;
extern NSString * ConnectionStatePropertyKey;
extern NSString * ReachablePropertyKey;
extern NSString * const LabelPropertyKey;
extern NSString * CurrentCommandPropertyKey;

extern const NSTimeInterval MonitorTaskTimeThreshold;

enum ErrorCodes
{
    ErrorServerUnreachable = 0
};

@interface P4Server : NSObject <NSCoding, P4ClientApiConnectionDelegate, P4ClientApiCommandDelegate> {
@private
    NSString     * label;
    
    P4ClientApi      * taggedConnection;
    ConnectionState    connectionState;
    NSOperationQueue * operationQueue;

    NSMutableArray   * commandQueue;
    NSDictionary     * _currentCommand;
    NSTimeInterval     lastUpdate;

    NSDictionary * cachedInfo;
    
    NSArray      * _cachedMonitorEntries;
    NSDictionary * _usersInMonitorTable;
    
    int            numberOfUsers;
    int            newNumberOfUsers;
    BOOL           currentUserExistsOnServer;
    BOOL           requiresTicket;
    
    NSString     * _cachedRemoteHostName;
    NSDate       * _cachedExpirationDate;
    
    NSTimeInterval refreshRate;
    NSTimer      * timer;
    NSString     * p4port;
    NSString     * user;
    NSString     * password;

    NSError * lastError;

    SCNetworkReachabilityRef _reachabilityRef;
    BOOL _reachable;
    
    BOOL _active;
    BOOL _discovered;
}

+(NSString*)defaultUser;
+(NSString*)apiDefaultUser;

-(id)init;
-(id)initAsDiscovered:(BOOL)discovered;
-(IBAction)refresh:(id)sender;
-(IBAction)updateInfo:(id)sender;
-(IBAction)updateMonitorInfo:(id)sender;
-(IBAction)updateUsers:(id)sender;

@property (readonly, assign)  BOOL             reachable;

@property (readwrite, copy)   NSString       * p4port;
@property (readonly)          NSString       * hostname;
@property (readonly)          NSString       * remoteHostname;
@property (readwrite, copy)   NSString       * label;
@property (readwrite, copy)   NSString       * user;
@property (readwrite, copy)   NSString       * password;

@property (readwrite, assign) NSTimeInterval   refreshRate;
@property (readonly, copy)    NSDictionary   * currentCommand;
@property (readonly)          NSString       * currentCommandDescription;

@property (readonly) NSDictionary    * info;
@property (readonly, copy) NSDictionary    * usersInMonitorTable;
@property (readonly, copy) NSArray         * monitorInfo;
@property (readonly) int               numberOfUsers;
@property (readonly) BOOL              currentUserExistsOnServer;
@property (readonly) BOOL              hasAlert;
@property (readonly) NSError         * lastError;
@property (readonly) BOOL              active;
@property (readonly) int               maximumUserCount;
@property (readonly) NSTimeInterval    remainingLicenseTime;
@property (readonly) BOOL              hasLicense;
@property (readonly) NSString        * simpleVersion;
@property (readonly) NSString        * platform;
@property (readonly) NSInteger         numberofIdleProcesses;
@property (readonly) NSArray         * activeProcesses;
@property (readonly) NSArray         * idleProcesses;
@property (readonly) ConnectionState   connectionState;
@property (readonly) BOOL              unicodeEnabled;
@property (readonly) BOOL              monitorEnabled;
@property (readonly) BOOL              requiresTicket;

@property (readonly) BOOL              discovered;


@end

NSTimeInterval TimeIntervalFromString( NSString * s );
