//
//  FirstRunViewController.m
//  P4Scout
//
//  Created by Michael Bishop on 8/7/09.
//  Copyright 2009 Perforce Software. All rights reserved.
//

#import "FirstRunViewController.h"
#import "P4Server.h"
#import "P4DefaultsKeys.h"


@implementation FirstRunViewController
@synthesize defaultUserTextField;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.defaultUserTextField.placeholder = [P4Server apiDefaultUser];
    self.defaultUserTextField.delegate = self;
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (IBAction)dismiss:(id)sender
{
    if ( ![self.defaultUserTextField.text isEqualToString:[P4Server defaultUser]] )
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:self.defaultUserTextField.text forKey:kDefaultUserPreferenceKey];
    }

    [self.parentViewController dismissModalViewControllerAnimated:YES];
}


-(BOOL)textFieldShouldReturn:(id)sender
{
    [self.defaultUserTextField resignFirstResponder];
    [self dismiss:self];
    return YES;
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
