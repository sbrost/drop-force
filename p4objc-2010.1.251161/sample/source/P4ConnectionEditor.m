//
//  SingleServerTableController.m
//  p4scout
//
//  Created by Work on 8/30/08.
//  Copyright 2008 Perforce Software, Inc. All rights reserved.
//

#import "P4ConnectionEditor.h"
#import "P4Server.h"
#import "p4scoutAppDelegate.h"

#define PORT_INDEX     0
#define USER_INDEX     1
#define PASSWORD_INDEX 2
#define LABEL_INDEX    3
const int LABEL_TAG = 4;
const int VALUE_TAG = 5;

// Uncomment this to update the suggested label dynamically when entering
// server information in the form.
//#define SHOW_SUGGESTED_LABEL

enum TableSections {
    ConnectionFields,
    DeleteButton
};

@interface P4ConnectionEditor ()

@property (readwrite, retain) UIView * fakeFirstResponder;

-(void)updateLabelPlaceholderText;
-(NSString*)labelPlaceHolderText;

@end

@implementation P4ConnectionEditor


@synthesize server, isNewServer, fakeFirstResponder, deleteButton;

-(id)initWithCoder:(NSCoder*)coder
{
    if ( (self = [super initWithCoder:coder]) == nil )
        return nil;
    
    return self;
}


- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if (self = [super initWithStyle:style]) {
    }
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view.
- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)updateLabelPlaceholderText
{
    UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:LABEL_INDEX inSection:0]];
    UITextField * tf = (UITextField *)[cell.contentView viewWithTag:VALUE_TAG];
    tf.placeholder = [self labelPlaceHolderText];
}

-(NSString*)labelPlaceHolderText
{
    NSString * placeholderText = server.p4port;

#ifdef SHOW_SUGGESTED_LABEL
    placeholderText = [server.info objectForKey:@"serverName"];
    if ( !placeholderText )
        placeholderText = [server.info objectForKey:@"serverAddress"];
    
    if ( !placeholderText )
        placeholderText = server.p4port;
#endif
    
    if ( !placeholderText )
        placeholderText = @"perforce:1666";
    return [NSString stringWithFormat:@"Label (%@)", placeholderText];
}

-(void)setServer:(P4Server*)s
{
    [server removeObserver:self forKeyPath:InfoPropertyKey];

    [s retain];
    [server release];
    server = s;

    [server addObserver:self forKeyPath:InfoPropertyKey options:NSKeyValueObservingOptionNew context:NULL];
    [self.tableView reloadData];
}


-(void)observeValueForKeyPath:(NSString *)key
                     ofObject:(id)object
                       change:(NSDictionary *)dict
                      context:(void *)context
{
    NSAssert( [key isEqualToString:InfoPropertyKey], @"Unrecognized key" );
    [self updateLabelPlaceholderText];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ( deleteButton )
        return 2;
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if ( section == DeleteButton )
    {
        NSAssert(deleteButton, @"DeleteButton row requested bu no delete button!");
        return 1;
    }
    return 4;
}


-(void)viewWillDisappear:(BOOL)animated
{
    [self.fakeFirstResponder resignFirstResponder]; // tell it to finish editing
    [super viewWillDisappear:animated];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.fakeFirstResponder = textField; // keep track of the first responder
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    const int tag = [[textField superview] tag];

    NSString * text = textField.text;
    if ( [text length] == 0 )
        text = nil;

    switch (tag) {
        case PORT_INDEX:
            server.p4port = text;
#ifdef SHOW_SUGGESTED_LABEL
            [server updateInfo:self];
#else
            [self updateLabelPlaceholderText];
#endif
            break;
        case USER_INDEX:
            server.user = text;
            break;
        case PASSWORD_INDEX:
            server.password = text;
            break;
        case LABEL_INDEX:
            server.label = text;
            break;
        default:
            NSAssert( NO, ([NSString stringWithFormat:@"TextField with unregognized tag:%d asked to commit text: %@", tag, textField.text]));
            break;
    }
    self.fakeFirstResponder = nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ServerParameter";
    static NSString *DeleteButtonCellIdentifier = @"DeleteButtonCellIdentifier";
    
    if ( indexPath.section == DeleteButton )
    {
        NSAssert(deleteButton, @"DeleteButton row requested but no delete button!");
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DeleteButtonCellIdentifier];
        if (cell == nil) {
            CGRect frame = CGRectMake(0, 0, 300, 44);
            cell = [[[UITableViewCell alloc] initWithFrame:frame
                                           reuseIdentifier:DeleteButtonCellIdentifier] autorelease];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            deleteButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
            deleteButton.frame = cell.contentView.frame;
            [cell.contentView addSubview:deleteButton];
        }
        return cell;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell == nil) {
        CGRect frame = CGRectMake(0, 0, 300, 44);
        cell = [[[UITableViewCell alloc] initWithFrame:frame
                                       reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UITextField * tf = [[UITextField alloc] initWithFrame:CGRectInset( cell.contentView.frame, 10, 10 )];
        tf.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        tf.tag = VALUE_TAG;
        tf.delegate = self;
        [cell.contentView addSubview:tf];
        [tf release];
    }
    
    UITextField * tf = (UITextField *)[cell.contentView viewWithTag:VALUE_TAG];
//    NSLog(@"ViewTag %d is view object %@", VALUE_TAG, tf);    
    
    tf.secureTextEntry = NO;
    tf.clearButtonMode = UITextFieldViewModeWhileEditing;
    tf.autocapitalizationType = UITextAutocapitalizationTypeNone;

    if (indexPath.row == PORT_INDEX)
    {
        cell.contentView.tag = PORT_INDEX;
        tf.placeholder = @"Port (perforce:1666)";
        tf.text = server.p4port;
        
        if ( server.discovered )
            tf.text = [server.info objectForKey:@"serverAddress"];

        tf.keyboardType = UIKeyboardTypeURL;
        tf.autocorrectionType = UITextAutocorrectionTypeNo;
        tf.userInteractionEnabled = YES;
    }
    else if (indexPath.row == USER_INDEX)
    {
        cell.contentView.tag = USER_INDEX;
        tf.placeholder = [NSString stringWithFormat:@"User (%@)", [P4Server defaultUser]];
        tf.text = server.user;
        tf.keyboardType = UIKeyboardTypeASCIICapable;
        tf.autocorrectionType = UITextAutocorrectionTypeNo;
        tf.userInteractionEnabled = YES;
    }
    else if (indexPath.row == PASSWORD_INDEX)
    {
        cell.contentView.tag = PASSWORD_INDEX;
        tf.placeholder = @"Password";
        tf.text = server.password;
        tf.secureTextEntry = YES;
        tf.keyboardType = UIKeyboardTypeDefault;
        tf.autocorrectionType = UITextAutocorrectionTypeNo;
        tf.userInteractionEnabled = YES;
    }
    else if (indexPath.row == LABEL_INDEX)
    {
        cell.contentView.tag = LABEL_INDEX;
        tf.placeholder = [self labelPlaceHolderText];
        tf.text = server.label;
        tf.keyboardType = UIKeyboardTypeDefault;
        tf.autocorrectionType = UITextAutocorrectionTypeYes;
        tf.userInteractionEnabled = YES;
        tf.autocapitalizationType = UITextAutocapitalizationTypeWords;
    }
    // Configure the cell
    return cell;
}

/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}
*/

/*
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
    }
    if (editingStyle == UITableViewCellEditingStyleInsert) {
    }
}
*/

/*
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
}
*/
/*
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
*/

- (void)dealloc {
    [server removeObserver:self forKeyPath:InfoPropertyKey];
    [server release];
    [super dealloc];
}


@end

