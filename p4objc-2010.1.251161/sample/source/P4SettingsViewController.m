//
//  P4SettingsViewController.m
//  P4Scout
//
//  Created by Michael Bishop on 8/12/09.
//  Copyright 2009 Perforce Software. All rights reserved.
//

#import "P4SettingsViewController.h"
#import "P4DefaultsKeys.h"
#import "P4Server.h"

enum SettingsCells {
    DefaultUserCell = 0,
    ZeroConfCell,
    TotalCells
};

static const int kDefaultUserCell = 0;
static const int kZeroConfCell = 1;

@implementation P4SettingsViewController

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.topViewController.tableView.selectionStyle = UITableViewCellSelectionStyleNone;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    zeroconfSwitch.on = [defaults boolForKey:kServerDiscoveryPreferenceKey];

    defaultUserTextField.text = [defaults objectForKey:kDefaultUserPreferenceKey];
    defaultUserTextField.placeholder = [P4Server apiDefaultUser];
    defaultUserTextField.delegate = self;
    
    tableView.allowsSelection = NO;
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ( section != 0 )
        return 0;
    return TotalCells;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ( indexPath.section != 0 )
        return nil;

    if ( indexPath.row == DefaultUserCell )
        return defaultUserCell;
    
    if ( indexPath.row == ZeroConfCell )
        return zeroconfCell;
        
    return nil;
}

- (IBAction)dismiss:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    [defaults setObject:defaultUserTextField.text forKey:kDefaultUserPreferenceKey];

    [defaults setBool:zeroconfSwitch.on forKey:kServerDiscoveryPreferenceKey];

    [self.parentViewController dismissModalViewControllerAnimated:YES];
}

-(BOOL)textFieldShouldReturn:(id)sender
{
    [defaultUserTextField resignFirstResponder];
    return YES;
}



@end
