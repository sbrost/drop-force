//
//  AboutBoxViewController.h
//  P4Scout
//
//  Created by Michael Bishop on 3/10/09.
//  Copyright 2009 Perforce Software. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AboutBoxViewController : UIViewController <UIWebViewDelegate> {
    IBOutlet UIWebView * webview;
    IBOutlet UIBarButtonItem * doneButton;
}

-(BOOL)webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
navigationType:(UIWebViewNavigationType)navigationType;

-(IBAction)close:(id)sender;

@end
