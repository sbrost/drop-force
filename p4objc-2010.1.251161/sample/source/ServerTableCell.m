//
//  ServerTableCell.m
//  p4scout
//
//  Created by Work on 8/30/08.
//  Copyright 2008 Perforce Software, Inc. All rights reserved.
//

#import "ServerTableCell.h"


@implementation ServerTableCell

@synthesize serverLabel, activityView;


//- (id)initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier {
//    if (self = [super initWithFrame:frame reuseIdentifier:reuseIdentifier]) {
//        // Initialization code
//    }
//    serverLabel = [[UILabel alloc] initWithFrame:frame];
//    serverLabel.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
//    [self addSubview:serverLabel];
//    userLabel = [[UILabel alloc] initWithFrame:frame];
//    userLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
//    [self addSubview:userLabel];
//    return self;
//}


//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}


//- (void)dealloc {
//    [userLabel release];
//    [serverLabel release];
//    [super dealloc];
//}
//
//- (void)layoutSubviews
//{
//    CGRect r = self.contentView.bounds;
//    r.size.height /= 2.0;
//    r = CGRectInset(r, 4, 4 );
//    serverLabel.frame = r;
//    r.origin.y += self.contentView.bounds.size.height / 2.0;
//    userLabel.frame = r;
//}
//
//UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//if (cell == nil) {
//    CGRect frame = CGRectMake(0, 0, 300, 44);
//    cell = [[[UITableViewCell alloc] initWithFrame:frame
//                                   reuseIdentifier:CellIdentifier] autorelease];
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    
//    label = [[[UILabel alloc] initWithFrame:CGRectMake(0.0, 12.0, 70.0, 25.0)] autorelease];
//    label.tag = LABEL_TAG;
//    label.font = [UIFont systemFontOfSize:12.0];
//    label.textAlignment = UITextAlignmentRight;
//    label.textColor = [UIColor blueColor];
//    label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
//    [cell.contentView addSubview:label];
//    
//    value = [[[UITextField alloc] initWithFrame:CGRectMake(75.0, 12.0, 200.0, 25.0)] autorelease];
//    value.tag = VALUE_TAG;
//    value.font = [UIFont systemFontOfSize:12.0];
//    value.textColor = [UIColor blackColor];
//    value.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//    [cell.contentView addSubview:value];
//} else {
//    label = (UILabel *)[cell.contentView viewWithTag:LABEL_TAG];
//    value = (UITextField *)[cell.contentView viewWithTag:VALUE_TAG];
//}

-(void)dealloc
{
    [serverLabel release];
    [activityView release];
    [super dealloc];
}

@end
