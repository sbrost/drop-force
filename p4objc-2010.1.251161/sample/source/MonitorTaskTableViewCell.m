//
//  MonitorTaskTableViewCell.m
//  p4scout
//
//  Created by Work on 12/15/08.
//  Copyright 2008 Perforce Software, Inc. All rights reserved.
//

#import "MonitorTaskTableViewCell.h"


@implementation MonitorTaskTableViewCell
@synthesize userLabel,fullNameLabel,commandLabel,timeLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)dealloc {
    [super dealloc];
}


@end
