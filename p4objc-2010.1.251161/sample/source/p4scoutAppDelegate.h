//
//  p4scoutAppDelegate.h
//  p4scout
//
//  Created by Work on 3/13/08.
//  Copyright Perforce Software, Inc. 2008. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ServerTableController;
@class P4SettingsViewController;

@interface p4scoutAppDelegate : NSObject {

@private
    IBOutlet UIWindow * window;
    IBOutlet UINavigationController * navigationController;
    IBOutlet ServerTableController * serverTableController;
    IBOutlet P4SettingsViewController * settingsViewController;
    IBOutlet UINavigationController * aboutBoxContainer;
    
}


-(IBAction)showAboutBox:(id)sender;
-(IBAction)closeAboutBox:(id)sender;
-(IBAction)showDefaultUserDialog:(id)sender;
-(IBAction)showSettings:(id)sender;

@end
