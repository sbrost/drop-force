//
//  UIToolbarAdditions.h
//  p4scout
//
//  Created by Work on 1/27/09.
//  Copyright 2009 Perforce Software, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIToolbar (UIToolbarAdditions)

-(void)insertItem:(UIBarItem *)item inItemsAtIndex:(int)index animated:(BOOL)animated;
-(void)removeItemFromItemsAtIndex:(int)index animated:(BOOL)animated;
-(void)removeItem:(UIBarItem*)item fromItemsAnimated:(BOOL)animated;

@end
