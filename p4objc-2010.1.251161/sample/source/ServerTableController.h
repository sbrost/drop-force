//
//  ServerTableController.h
//  p4scout
//
//  Created by Work on 8/30/08.
//  Copyright 2008 Perforce Software, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@class P4ConnectionEditor;
@class P4Server;

@interface ServerTableController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate> {
    IBOutlet UINavigationController * serverAddDialogController;
    IBOutlet P4ConnectionEditor * connectionEditorController;

    IBOutlet UIActivityIndicatorView * activityIndicatorView;
    IBOutlet UIToolbar * toolbar;
    IBOutlet UIBarItem * activityIndicatorViewItem;
    IBOutlet UILabel * statusLabel;
    UIBarItem * refreshButton;
    BOOL browsing;

@private
    NSMutableArray * _servers;
    NSMutableArray * foundServers;
    int              numberOfActiveServers;
    UITableView * _tableView;
    NSNetServiceBrowser * serverBrowser;
}
@property (readwrite, copy) NSArray * servers;
@property (readwrite, retain) IBOutlet UITableView * tableView;
@property (readwrite, retain) IBOutlet UIBarItem * refreshButton;
@property (readwrite, assign) BOOL browsing;

-(void)addServer:(P4Server *)s;
-(void)removeServer:(P4Server *)s;

-(IBAction)cancel:(id)sender;
-(IBAction)save:(id)sender;

-(IBAction)add:(id)sender;
-(IBAction)refresh:(id)sender;

@end
