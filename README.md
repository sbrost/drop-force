# README #

Drop Force is simple drag and drop Perforce client that is easy to use from the Finder or Xcode.

It uses the Peforce C/C++ and Objective-C APIS (http://www.perforce.com/product/components/apis)

![Alt text](https://bytebucket.org/sbrost/drop-force/raw/da4fe25e91ee78c5d7b5c3c1d1b17f222090dd6b/Screenshot%202.png)
![Alt text](https://bytebucket.org/sbrost/drop-force/raw/9a0fedd6ac643ffb184c8029c4abab5b63c91837/Screenshot%201.png)
