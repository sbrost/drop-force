//
//  Drop_ForceTests.m
//  Drop ForceTests
//
//  Created by Steve Brost on 11/23/16.
//  Copyright © 2016 Software Pot Pie. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DFChangelistDescription.h"

@interface Drop_ForceTests : XCTestCase

@end

@implementation Drop_ForceTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testDFChangelistDescription
{
    /* KEYS
     change = 81330;
     changeType = public;
     client = "SteveB-MacBookPro";
     desc = "NEW\n";
     func = "client-FstatInfo";
     status = pending;
     time = 1479865080;
     user = steve;
     */
    //@{ @"hi" :  @"bork", @"greeble" :  @"bork" }
    NSDictionary* happyPath  = @{@"change" : @"81330",
                                 @"changeType":@"public",
                                 @"client":@"you",
                                 @"desc":@"this is a description",
                                 @"func":@"function",
                                 @"status":@"pending",
                                 @"time":@"12345",
                                 @"user":@"me"};
    DFChangelistDescription* desc = [[DFChangelistDescription alloc] initWithDictionaryResponce:happyPath];
    XCTAssertEqualObjects(desc.identifier, @"81330");
    XCTAssertEqualObjects(desc.type, @"public");
    XCTAssertEqualObjects(desc.client, @"you");
    XCTAssertEqualObjects(desc.changeDescription, @"this is a description");
    XCTAssertEqualObjects(desc.function, @"function");
    XCTAssertEqualObjects(desc.status, @"pending");
    XCTAssertEqualObjects(desc.time, @"12345");
    XCTAssertEqualObjects(desc.user, @"me");
    
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

//- (void)testPerformanceExample {
//    // This is an example of a performance test case.
//    [self measureBlock:^{
//        // Put the code you want to measure the time of here.
//    }];
//}

@end
