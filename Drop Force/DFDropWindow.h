//
//  DFDropWindow.h
//  Drop Force
//
//  Created by Steve Brost on 6/22/12.
//  Copyright (c) 2012 Software Pot Pie. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class DFCommandView;
@interface DFDropWindow : NSPanel

@property (nonatomic, strong) IBOutlet DFCommandView* editView;
@property (nonatomic, strong) IBOutlet DFCommandView* addView;
@property (nonatomic, strong) IBOutlet DFCommandView* revertView;
@property (nonatomic, strong) IBOutlet DFCommandView* deleteView;

@end
