//
//  DFConstants.m
//  Drop Force
//
//  Created by Steve Brost on 5/26/17.
//  Copyright © 2017 Software Pot Pie. All rights reserved.
//

#import "DFConstants.h"

NSString* const kDepotFileKey                        = @"depotFile";
NSString* const kClientFileKey                       = @"clientFile";
NSString* const kClientRootKey                       = @"clientRoot";

