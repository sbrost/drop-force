//
//  main.m
//  Drop Force
//
//  Created by Steve Brost on 6/22/12.
//  Copyright (c) 2012-2016 Software Pot Pie. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
