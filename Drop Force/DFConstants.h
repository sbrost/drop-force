//
//  DFConstants.h
//  Drop Force
//
//  Created by Steve Brost on 5/26/17.
//  Copyright © 2017 Software Pot Pie. All rights reserved.
//

#import <Cocoa/Cocoa.h>

extern NSString* const kDepotFileKey;
extern NSString* const kClientFileKey;
extern NSString* const kClientRootKey;

