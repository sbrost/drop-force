//
//  DFChangelistDescription.m
//  Drop Force
//
//  Created by Steve Brost on 11/23/16.
//  Copyright © 2016 Software Pot Pie. All rights reserved.
//

#import "DFChangelistDescription.h"

/*
 change = 81330;
 changeType = public;
 client = "SteveB-MacBookPro";
 desc = "NEW\n";
 func = "client-FstatInfo";
 status = pending;
 time = 1479865080;
 user = steve;
 */

@implementation DFChangelistDescription

- (instancetype)initWithDictionaryResponce:(NSDictionary *)dictionayRepsonce
{
    if ((self = [super init]))
    {
        _dictionaryDesc = dictionayRepsonce;
    }
    
    return self;
}

- (NSString*) identifier
{
    return _dictionaryDesc[@"change"];
}

- (NSString*) type
{
    return _dictionaryDesc[@"changeType"];
}

- (NSString*) client
{
    return _dictionaryDesc[@"client"];
}

- (NSString*) changeDescription
{
    return _dictionaryDesc[@"desc"];
}

- (NSString*) function
{
    return _dictionaryDesc[@"func"];
}

- (NSString*) status
{
    return _dictionaryDesc[@"status"];
}

- (NSString*) time
{
    return _dictionaryDesc[@"time"];
}

- (NSString*) user
{
    return _dictionaryDesc[@"user"];
}


@end
