//
//  DFDropWindow.m
//  Drop Force
//
//  Created by Steve Brost on 6/22/12.
//  Copyright (c) 2012 Software Pot Pie. All rights reserved.
//

#import "DFDropWindow.h"
#import "DFCommandView.h"

@implementation DFDropWindow
@synthesize editView, revertView, deleteView, addView;

- (void)awakeFromNib
{
    NSColor* addTileColor    = [NSColor colorWithDeviceRed:.17 green:.48 blue:.73 alpha:1.0];
    NSColor* editTileColor   = [NSColor colorWithDeviceRed:.60 green:.78 blue:.40 alpha:1.0];//[NSColor colorWithDeviceRed:.43 green:.77 blue:.82 alpha:1.0];
    NSColor* revertTileColor = [NSColor colorWithDeviceRed:.93 green:.56 blue:.13 alpha:1.0];
    NSColor* deleteTileColor = [NSColor colorWithDeviceRed:.87 green:.37 blue:.22 alpha:1.0];
    
    NSMutableDictionary* titleAttributes = [NSMutableDictionary new];
    [titleAttributes setObject:[NSColor whiteColor] forKey: NSForegroundColorAttributeName];
    [titleAttributes setObject:[NSFont boldSystemFontOfSize:18] forKey:NSFontAttributeName];
    
    self.addView.title = NSLocalizedString(@"Add", @"Add");
    self.addView.backgroundColor = addTileColor;
    self.addView.titleAttributes = titleAttributes;
    self.addView.image = [NSImage imageNamed:@"add"];
    self.addView.command = @"add";
    
    self.editView.title = NSLocalizedString(@"Edit", @"Edit");
    self.editView.backgroundColor = editTileColor;
    self.editView.titleAttributes = titleAttributes;
    self.editView.image = [NSImage imageNamed:@"edit"];
    self.editView.command = @"edit";

    
    self.revertView.title = NSLocalizedString(@"Revert", @"Revert");
    self.revertView.backgroundColor = revertTileColor;
    self.revertView.titleAttributes = titleAttributes;
    self.revertView.image = [NSImage imageNamed:@"revert"];
    self.revertView.command = @"revert";
    
    self.deleteView.title = NSLocalizedString(@"Delete", @"Delete");
    self.deleteView.backgroundColor = deleteTileColor;
    self.deleteView.titleAttributes = titleAttributes;
    self.deleteView.image = [NSImage imageNamed:@"delete"];
    self.deleteView.command = @"delete";


}

- (void)dealloc
{
    self.editView = nil;
    self.revertView = nil;
    self.deleteView = nil;
    self.addView = nil;
}

@end
