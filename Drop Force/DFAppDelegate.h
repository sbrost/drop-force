//
//  DFAppDelegate.h
//  Drop Force
//
//  Created by Steve Brost on 6/22/12.
//  Copyright (c) 2012-2016 Software Pot Pie. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class DFChangelistDescription;
@class DFPendingChangeListDescWindowController;

@interface DFAppDelegate : NSObject <NSApplicationDelegate>
{
@private
    NSWindowController*     _settingsController;
    DFPendingChangeListDescWindowController* _pendingChangelistDescController;
    
    BOOL _creatingANewChangelist;
    
// not quite sold on auto refreshing yet, or what the interval should be
//    NSTimer*                _changelistRefreshTimer;
}


+ (DFChangelistDescription*)currentChangelist;

@property (assign) IBOutlet NSWindow* window;
@property (assign) IBOutlet NSPopUpButton* changelistPicker;

@end
