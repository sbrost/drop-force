//
//  P4ClientApi+Singleton.m
//  Drop Force
//
//  Created by Steve Brost on 11/22/16.
//  Copyright © 2016 Software Pot Pie. All rights reserved.
//

#import "P4ClientApi+Singleton.h"

NSString* const kP4User     = @"P4USER";
NSString* const kP4Password = @"P4CLIENT";
NSString* const kP4Client   = @"P4CLIENT";
NSString* const kP4Host     = @"P4HOST";
NSString* const kP4PORT     = @"P4PORT";

@implementation P4ClientApi (Singleton)

+ (P4ClientApi*)createNewClient
{
    P4ClientApi* client = client = [P4ClientApi new];
    if (!client.connected)
    {
        NSString* port = [[NSUserDefaults standardUserDefaults] stringForKey:kP4PORT];
        NSError*  error;
        if (![client connectToPort:port withProtocol:nil error:&error])
        {
            [client showConnectionError:error];
            @throw [NSException exceptionWithName:@"Connection Error" reason:[error description] userInfo:nil];
        }
        else
        {
            client.user = [[NSUserDefaults standardUserDefaults] stringForKey:kP4User];
            client.password = [[NSUserDefaults standardUserDefaults] stringForKey:kP4Password];
            client.client = [[NSUserDefaults standardUserDefaults] stringForKey:kP4Client];
            client.hostname = [[NSUserDefaults standardUserDefaults] stringForKey: kP4Host];
        }
        
    }
    return client;
}

- (void)showConnectionError:(NSError*)error
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:kShowErroNotificationKey])
        return;
    
    NSUserNotification* errorNotification = [NSUserNotification new];
    
    NSString* titleString = NSLocalizedString(@"Connection Error", @"Connection Error");
    errorNotification.title = titleString;
    
    errorNotification.informativeText = [error localizedDescription];
    errorNotification.deliveryDate = [NSDate date];
    
    [[NSUserNotificationCenter defaultUserNotificationCenter] scheduleNotification: errorNotification];
}

-(void)clientApiDidConnect:(P4ClientApi*)api
{
    NSLog(@"successful connection to perforce serter");
}

-(void)clientApi:(P4ClientApi*)api didFailToConnectWithError:(NSError*)error
{
    NSLog(@"connect failure - %@", error);
}

-(void)clientApi:(P4ClientApi*)api didDisconnectWithError:(NSError *)error
{
    NSLog(@"disconnect with failure - %@", error);
    
}

- (void)runCommandInBlock:(NSString *)command
            withArguments:(NSArray *)args
                 delegate:(id<P4ClientApiCommandDelegate>)delegate
{
    NSBlockOperation* commandOperation = [NSBlockOperation blockOperationWithBlock:^
      {
          @try
          {
              //P4ClientApi* p4Client = [P4ClientApi createNewClient];
              [self runCommand:command withArguments:args delegate:delegate];
          }
          @catch(NSException* e)
          {
              
          }
          @finally
          {
              
          }
      }];
    
    //[_commandQueue addOperation:commandOperation];
    [[NSOperationQueue mainQueue] addOperation:commandOperation];
    
}

+ (void)runCommandAsync:(NSString*)command
            withArguments:(NSArray*)args
                 delegate:(id<P4ClientApiCommandDelegate>)delegate
{
    NSString* command_label = [NSString stringWithFormat:@"%@.command.%@", [NSBundle mainBundle].bundleIdentifier, command];
    dispatch_queue_t command_queue = dispatch_queue_create([command_label cStringUsingEncoding:NSASCIIStringEncoding], /*NULL*/DISPATCH_QUEUE_SERIAL);
    /*ARC will release the dispatch_queue ... so says the documentation*/
    
    dispatch_async(command_queue, ^
    {
        @try
        {
            P4ClientApi* p4Client = [P4ClientApi createNewClient];
            [p4Client runCommand:command withArguments:args delegate:delegate];
        }
        @catch(NSException* e)
        {
            
        }
        @finally
        {
            
        }
    });
 }

@end
