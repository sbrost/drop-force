//
//  DFCommandView.m
//  Drop Force
//
//  Created by Steve Brost on 6/22/12.
//  Copyright (c) 2012-2016 Software Pot Pie. All rights reserved.
//

#import "DFCommandView.h"
#import "P4ClientApi+Singleton.h"
#import "DFChangelistDescription.h"
#import "DFAppDelegate.h"
#import "DFNotifications.h"

@interface DFCommandView (PrivateCommandDelegate)<P4ClientApiCommandDelegate>

@end

@implementation DFCommandView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        [self awakeFromNib];
    }
    
    return self;
}

- (void)awakeFromNib
{
    [self registerForDraggedTypes: [self registeredDraggedTypes]];
}

- (void)drawRect:(NSRect)dirtyRect
{
    [NSBezierPath setDefaultLineWidth:5.0f];
    static const CGFloat kRedComponent = .92;
    static const CGFloat kGreenComponent = .91;
    static const CGFloat kBlueComponent = .86;
    static const CGFloat kAlphaComponent = 1.0;
    [[NSColor colorWithDeviceRed:kRedComponent green:kGreenComponent blue:kBlueComponent alpha:kAlphaComponent] set];
    [NSBezierPath strokeRect:self.bounds];
    
    
    [self.backgroundColor set];
    static const CGFloat kWidth = 4.0;
    static const CGFloat kHeight = kWidth;
    NSRect fillRect = NSInsetRect(self.bounds, kWidth, kHeight);
    [NSBezierPath fillRect:fillRect];
    NSRect strokeRect = NSInsetRect(fillRect, -1, -1);
    [[NSColor colorWithDeviceRed:.86 green:.85 blue:.82 alpha:1.0] set];
    [NSBezierPath strokeRect:strokeRect];
    
    NSRect textRect = NSInsetRect(fillRect, 10.0f, 10.0f);
    [self.title drawWithRect:textRect options:NSStringDrawingTruncatesLastVisibleLine attributes:self.titleAttributes];
    
    NSRect imageRect = self.bounds;
    imageRect.size = NSMakeSize(32, 32);
    imageRect.origin = NSMakePoint(self.bounds.size.width -32 - 8, self.bounds.size.height - 32-8);
    
    [self.image drawInRect:imageRect fromRect:NSMakeRect(0, 0, 32, 32) operation:NSCompositingOperationSourceOver fraction:1.0];
}

- (NSArray*)registeredDraggedTypes
{
    return [NSArray arrayWithObject:NSFilenamesPboardType];
}

- (NSDragOperation)draggingEntered:(id )sender
{
    if ((NSDragOperationCopy & [sender draggingSourceOperationMask]) == NSDragOperationCopy) 
        return NSDragOperationCopy;

    // not a drag we can use
    return NSDragOperationNone;
    
}

- (BOOL)prepareForDragOperation:(id )sender
{
    return YES;
}

- (void)performCommandOnFiles:(NSArray*)files
{
    P4ClientApi* p4Client = [P4ClientApi createNewClient];
    
    NSMutableArray* arguments = [NSMutableArray new];
    DFChangelistDescription* changelist = [DFAppDelegate currentChangelist];
    if (changelist != nil) /*not the "Default" changelist*/
    {
        [arguments addObject: @"-c"];
        [arguments addObject: changelist.identifier];
    }
    [arguments addObjectsFromArray: files];
    
    [p4Client runCommandInBlock:self.command withArguments:arguments delegate:self];
 }

- (BOOL)performDragOperation:(id )sender
{
    NSPasteboard* pasteboard = [sender draggingPasteboard];
    NSArray* typesArray = [NSArray arrayWithObject: NSFilenamesPboardType];
    
    NSString* desiredType = [pasteboard availableTypeFromArray:typesArray];
    
    if ([desiredType isEqualToString:NSFilenamesPboardType]) 
    {
        // the pasteboard contains a list of file names
        //Take the first one
        NSArray* fileNamesArray = [pasteboard propertyListForType:@"NSFilenamesPboardType"];
        
        [self setNeedsDisplay:YES];
        [self performCommandOnFiles:fileNamesArray];
        return YES;
        
    }// end if
    
    //this cant happen ???
    NSLog(@"wrong type: %@", desiredType);
    return NO;
}

@end

@implementation DFCommandView (PrivateCommandDelegate)

-(void)clientApi:(P4ClientApi*)api didReceiveError:(NSError*)error
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:kShowErroNotificationKey])
        return;
    
    NSUserNotification* errorNotification = [NSUserNotification new];
    
    NSString* titleString = [NSString stringWithFormat: @"%@%@", NSLocalizedString(@"Could not ", @"Could not "), self.command];
    errorNotification.title = titleString;
    
    errorNotification.informativeText = [error localizedDescription];
    errorNotification.deliveryDate = [NSDate date];
   
    [[NSUserNotificationCenter defaultUserNotificationCenter] scheduleNotification: errorNotification];
}
-(void)clientApi:(P4ClientApi*)api didReceiveSimpleServerMessage:(NSString*)message level:(char)level
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:kShowSuccessNotificaionKey])
        return;
    
    NSUserNotification* successNotification = [NSUserNotification new];
    
    NSString* titleString = _command;
    successNotification.title = titleString;
    successNotification.informativeText = message;
    
    successNotification.deliveryDate = [NSDate date];
    
    [[NSUserNotificationCenter defaultUserNotificationCenter] scheduleNotification: successNotification];

}
-(void)clientApi:(P4ClientApi*)api didReceiveTaggedResponse:(NSDictionary*)response
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:kShowSuccessNotificaionKey])
        return;
    
    NSUserNotification* successNotification = [NSUserNotification new];
    
    NSString* titleString = _command;
    successNotification.title = titleString;
    successNotification.informativeText = [NSString stringWithFormat:@"%@ %@", [response objectForKey:@"action"], [response objectForKey:@"clientFile"]];
    
    successNotification.deliveryDate = [NSDate date];
    
    [[NSUserNotificationCenter defaultUserNotificationCenter] scheduleNotification: successNotification];
    
}
-(void)clientApi:(P4ClientApi*)api didReceiveBinaryContent:(NSData*)data{NSLog(@"%@", data);}
-(void)clientApi:(P4ClientApi*)api didReceiveTextContent:(NSString*)text
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:kShowSuccessNotificaionKey])
        return;
    
    NSUserNotification* successNotification = [NSUserNotification new];
    
    NSString* titleString = _command;
    successNotification.title = titleString;
    successNotification.informativeText = text;
    
    successNotification.deliveryDate = [NSDate date];
    
    [[NSUserNotificationCenter defaultUserNotificationCenter] scheduleNotification: successNotification];
    

}
-(void)clientApiDidFinishCommand:(P4ClientApi*)api
{
    [[NSNotificationCenter defaultCenter] postNotificationName: kFileActionNotification object: api.currentCommand];
}

@end

