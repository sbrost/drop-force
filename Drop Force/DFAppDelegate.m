//
//  DFAppDelegate.m
//  Drop Force
//
//  Created by Steve Brost on 6/22/12.
//  Copyright (c) 2012 Software Pot Pie All rights reserved.
//

#import "DFAppDelegate.h"
#import "P4ClientApi+Singleton.h"
#import "DFNotifications.h"
#import "DFChangelistDescription.h"
#import "DFNewChangelistEditorWindowController.h"
#import "DFPendingChangeListDescWindowController.h"
#import "DFConstants.h"

NSString* const kGetChangelistsCommand               = @"changelists";
NSString* const kGetOpenedFilesCommand               = @"opened";
NSString* const kSubmitChangelistCommand             = @"submit";
NSString* const kAddFileCommand                      = @"add";
NSString* const kEditFileCommand                     = @"edit";
NSString* const kRevertFileCommand                   = @"revert";
NSString* const kDeleteFileCommand                   = @"delete";
NSString* const kSyncCommand                         = @"sync";


NSString* const kInputSwitch = @"-i";



const NSInteger kChangeListPopupTag = 55;
const NSInteger kShowChangelistButtonTag = 56;
const NSInteger kAppTag = -11;

static __weak DFAppDelegate* _sDFAppDelegate = nil;

@interface DFAppDelegate (CommandDelegate)<P4ClientApiCommandDelegate>


@end

@implementation DFAppDelegate

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _creatingANewChangelist = NO;
        [[NSNotificationCenter defaultCenter] addObserverForName:kFileActionNotification object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note)
        {
            [self showPendingChangeListFiles:self];
        }];
    }
    return self;
}

- (NSInteger)tag
{
    return kAppTag;
}

- (void)populateChangeListsPicker/*refactor to clear the menu either in the command block, or as a put both in a serial queue*/
{
    if (_changelistPicker == nil || [[NSUserDefaults standardUserDefaults] stringForKey:kP4Client] == nil)
        return;
    
    //clear out any existing items for a refresh
    for (NSUInteger i = _changelistPicker.itemArray.count - 1; i > 0; --i)
    {
        [_changelistPicker removeItemAtIndex: i];
    }
    
    static NSString* const kStatusSwitch          = @"-s";
    static NSString* const kStatusPendingKind     = @"pending";
    static NSString* const kFullDescriptionSwitch = @"-l";
    
    NSMutableArray* arguments = [NSMutableArray arrayWithArray: @[kStatusSwitch,
                                                                  kStatusPendingKind,
                                                                  kFullDescriptionSwitch]];
    static NSString* const kClientNameSwitch = @"-c";
    [arguments addObject: kClientNameSwitch];
    [arguments addObject: [[NSUserDefaults standardUserDefaults] stringForKey:kP4Client]];
    
    [P4ClientApi runCommandAsync:kGetChangelistsCommand withArguments:arguments delegate:self];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSMutableDictionary* defaults = [NSMutableDictionary dictionary];
    [defaults setObject:[NSNumber numberWithBool:YES] forKey:kShowSuccessNotificaionKey];
    [defaults setObject:[NSNumber numberWithBool:YES] forKey:kShowErroNotificationKey];
    
    NSUserDefaults* standard = [NSUserDefaults standardUserDefaults];
    [standard registerDefaults:defaults];
    
    [self populateChangeListsPicker];

// not quite sold on auto refreshing yet, or what the interval should be
//    _changelistRefreshTimer = [NSTimer scheduledTimerWithTimeInterval:5 repeats:YES block:^(NSTimer * _Nonnull timer)
//    {
//        [self populateChangeListsPicker];
//    }];
    
    _sDFAppDelegate = self;
}

- (IBAction)refresheChangeListsPicker:(id)sender
{
    [self populateChangeListsPicker];
}

- (IBAction)settings:(id)sender
{
    if (_settingsController == nil)
    {
        _settingsController = [[NSWindowController alloc] initWithWindowNibName:kSettings];
        [_settingsController.window center];
    }
    [_settingsController.window makeKeyAndOrderFront:sender];
}

- (IBAction)syncToHeadRevision:(id)sender
{
    P4ClientApi* commandClient = [P4ClientApi createNewClient];
    [commandClient runCommandInBlock:kSyncCommand withArguments:nil delegate:self];
}

- (void)createNewChangelistWithDescription:(NSString*)description
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* clientName     = [defaults stringForKey:kP4Client];
    NSString* userName       = [defaults stringForKey: kP4User];

    static NSString* const changeListSpecTemplate = @"Change:\tnew\nClient:\t%@\nUser:\t%@\nStatus:\tnew\nDescription:\n\t%@";
    NSString* newChangeListSpec = [NSString stringWithFormat:changeListSpecTemplate, clientName, userName, description];

    P4ClientApi* commandClient = [P4ClientApi createNewClient];
    commandClient.currentInput = newChangeListSpec;

    static NSString* const kNewChangelistCommand = @"changelist";

    _creatingANewChangelist = YES;
    [commandClient runCommand:kNewChangelistCommand withArguments:@[kInputSwitch] delegate:self];
    [self populateChangeListsPicker];
}

- (IBAction)createNewChangelist:(id)sender
{
    DFNewChangelistEditorWindowController* editorWC = [DFNewChangelistEditorWindowController newChangelistEditor];
    [editorWC loadWindow];
    editorWC.descriptionLabel.stringValue = NSLocalizedString(@"Create new changelist with description:", @"Create new changelist with description:");
    editorWC.window.title = NSLocalizedString(@"Create New Changelist", @"Create New Changelist");
    [self.window beginSheet:editorWC.window completionHandler:^(NSModalResponse returnCode)
    {
        if (returnCode == NSModalResponseCancel)
            return;
        
        NSString* description = [editorWC.descriptionView string];
        [self createNewChangelistWithDescription: description];
    }];
}

- (IBAction)submitPendingChangeList:(id)sender
{
/*
 # A Perforce Change Specification.
 #
 #  Change:      The change number. 'new' on a new changelist.
 #  Date:        The date this specification was last modified.
 #  Client:      The client on which the changelist was created.  Read-only.
 #  User:        The user who created the changelist.
 #  Status:      Either 'pending' or 'submitted'. Read-only.
 #  Type:        Either 'public' or 'restricted'. Default is 'public'.
 #  Description: Comments about the changelist.  Required.
 #  ImportedBy:  The user who fetched or pushed this change to this server.
 #  Identity:    Identifier for this change.
 #  Jobs:        What opened jobs are to be closed by this changelist.
 #               You may delete jobs from this list.  (New changelists only.)
 #  Files:       What opened files from the default changelist are to be added
 #               to this changelist.  You may delete files from this list.
 #               (New changelists only.)
 
 Change:	new
 
 Client:	SteveB-MacBookPro
 
 User:	steve
 
 Status:	new
 
 Description:
	<enter description here>
 
 Files:
	//depot/branch/Apps/helloworld/foo.m	# edit

 */
    
    DFNewChangelistEditorWindowController* editorWC = [DFNewChangelistEditorWindowController newChangelistEditor];
    [editorWC loadWindow];
    editorWC.descriptionLabel.stringValue = NSLocalizedString(@"Submit changelist with description:", @"Submit changelist with description:");
    editorWC.window.title = NSLocalizedString(@"Submit Changelist", @"Submit Changelist");
    
    NSString* changeDescription = [[DFAppDelegate currentChangelist] changeDescription];
    if (changeDescription == nil)
        changeDescription = @"";
    [editorWC.descriptionView setString: changeDescription];
    DFAppDelegate* weakSelf = self;
    [self.window beginSheet:editorWC.window completionHandler:^(NSModalResponse returnCode)
     {
         if (returnCode == NSModalResponseCancel)
             return;
         
         [self showPendingChangeListFiles:self];
         
         NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
         NSString* clientName     = [defaults stringForKey:kP4Client];
         NSString* userName       = [defaults stringForKey: kP4User];
         
         static NSString* const submitChangeListTemplate = @"Change:\t%@\nClient:\t%@\nUser:\t%@\nStatus:\t%@\nDescription:\n\t%@\nFiles:";
         static NSString* const kNewChange = @"new";
         static NSString* const kPendingStatus = @"pending";
         
         BOOL isDefaultChangelist = [[DFAppDelegate currentChangelist] identifier] == nil;
         NSString* change = isDefaultChangelist ? kNewChange : [[DFAppDelegate currentChangelist] identifier];
         NSString* status = isDefaultChangelist ? kNewChange : kPendingStatus;
         NSString* description = editorWC.descriptionView.string;
         
         NSMutableString* submitChangeListSpec = [NSMutableString stringWithFormat:submitChangeListTemplate, change, clientName, userName, status, description];
         
         for (NSDictionary* pendingFile in weakSelf->_pendingChangelistDescController.pendingFiles)
         {
             [submitChangeListSpec appendFormat:@"\n\t%@", pendingFile[kDepotFileKey]];
         }
         
         P4ClientApi* commandClient = [P4ClientApi createNewClient];
         commandClient.currentInput = submitChangeListSpec;
         
         NSMutableArray* arguments = [NSMutableArray arrayWithArray:@[kInputSwitch]];
         if (!isDefaultChangelist)
         {
             [arguments insertObject:change atIndex:0];
             [arguments insertObject:@"-c" atIndex:0];
         }
         
         [commandClient runCommandInBlock:kSubmitChangelistCommand withArguments:@[kInputSwitch] delegate:self];
         
     }];
}

- (IBAction)showPendingChangeListFiles:(id)sender
{
    DFChangelistDescription* desc =  [DFAppDelegate currentChangelist];

    if (_pendingChangelistDescController == nil)
    {
        _pendingChangelistDescController = [DFPendingChangeListDescWindowController new];
    }

    [_pendingChangelistDescController resetForChangeList:desc];

    NSArray* commandArgs = @[@"-c", desc == nil ? @"default" : desc.identifier];
    
    P4ClientApi* commandClient = [P4ClientApi createNewClient];
    [commandClient runCommand:kGetOpenedFilesCommand withArguments:commandArgs delegate:self];
    
    if ([sender tag] == kShowChangelistButtonTag)/*only toggle visibility from the toolbar, not from the popup*/
        [_pendingChangelistDescController showWindow:self];
}

- (IBAction)changeListPickerDidChange:(id)sender
{
    [self showPendingChangeListFiles:sender];
}

+ (DFChangelistDescription*)currentChangelist
{
    return _sDFAppDelegate.changelistPicker.selectedItem.representedObject;
}

@end


@implementation DFAppDelegate (CommandDelegate)

-(void)clientApi:(P4ClientApi*)api didReceiveError:(NSError*)error
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:kShowErroNotificationKey])
        return;
    
    NSUserNotification* errorNotification = [NSUserNotification new];
    
    NSString* titleString = [NSString stringWithFormat: @"%@%@", NSLocalizedString(@"Could not ", @"Could not "), api.currentCommand];
    errorNotification.title = titleString;
    
    errorNotification.informativeText = [error localizedDescription];
    errorNotification.deliveryDate = [NSDate date];
    
    [[NSUserNotificationCenter defaultUserNotificationCenter] scheduleNotification: errorNotification];
}

-(void)clientApi:(P4ClientApi*)api didReceiveSimpleServerMessage:(NSString*)message level:(char)level
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:kShowSuccessNotificaionKey])
        return;
    
    NSUserNotification* successNotification = [NSUserNotification new];
    successNotification.informativeText = message;
    successNotification.deliveryDate = [NSDate date];
    [[NSUserNotificationCenter defaultUserNotificationCenter] scheduleNotification: successNotification];
}

-(void)clientApi:(P4ClientApi*)api didReceiveTaggedResponse:(NSDictionary*)response
{
    if ([api.currentCommand isEqualToString:kGetChangelistsCommand])
    {
        const NSInteger kNewItemIndex = 1;
        
        DFChangelistDescription* changelist = [[DFChangelistDescription alloc] initWithDictionaryResponce:response];
        
        NSString* changelistMenuTitle = [NSString stringWithFormat:@"%@ - %@",
                                         changelist.identifier,
                                         changelist.changeDescription];
        
        
        [_changelistPicker insertItemWithTitle: changelistMenuTitle atIndex: kNewItemIndex];
        NSMenuItem* item = [_changelistPicker itemAtIndex: kNewItemIndex];
        item.representedObject = changelist;
        
    }
    else if ([api.currentCommand isEqualToString:kGetOpenedFilesCommand])
    {
        NSDictionary* fileDesc = @{kDepotFileKey :  response[kDepotFileKey],
                                kClientFileKey: response[kClientFileKey]};
        [_pendingChangelistDescController addPendingFile: fileDesc];
    }
}

-(void)clientApi:(P4ClientApi*)api didReceiveBinaryContent:(NSData*)data{}
-(void)clientApi:(P4ClientApi*)api didReceiveTextContent:(NSString*)text{}
-(void)clientApiDidFinishCommand:(P4ClientApi*)api
{
    if ([api.currentCommand isEqualToString:kGetChangelistsCommand])
    {
        if (_creatingANewChangelist)
        {
            _creatingANewChangelist = NO;
            [_changelistPicker selectItem: _changelistPicker.lastItem];
        }
    }
    
    if ([api.currentCommand isEqualToString: kSubmitChangelistCommand])
    {
        [self populateChangeListsPicker];
        [self showPendingChangeListFiles:self];
        
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:kShowSuccessNotificaionKey])
        {
            NSUserNotification* successNotification = [NSUserNotification new];
            successNotification.title = api.currentCommand;
            successNotification.informativeText = NSLocalizedString(@"Submission successful.", @"Submission successful.");
            successNotification.deliveryDate = [NSDate date];
            [[NSUserNotificationCenter defaultUserNotificationCenter] scheduleNotification: successNotification];
        }
    }
    
}


@end
