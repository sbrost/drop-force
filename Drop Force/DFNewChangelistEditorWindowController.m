//
//  DFNewChangelistEditorWindowController.m
//  Drop Force
//
//  Created by Steve Brost on 11/23/16.
//  Copyright © 2016 Software Pot Pie. All rights reserved.
//

#import "DFNewChangelistEditorWindowController.h"

@interface DFNewChangelistEditorWindowController ()

@end

@implementation DFNewChangelistEditorWindowController

+ (instancetype)newChangelistEditor
{
    return [[DFNewChangelistEditorWindowController alloc] initWithWindowNibName:@"DFNewChangelistEditorWindowController"];
}

- (void)dealloc
{
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

-(IBAction)createNewChangelist:(id)sender
{
    [self.window.sheetParent endSheet:self.window returnCode:NSModalResponseOK];
}

- (IBAction)doNotCreateNewChangelist:(id)sender
{
    [self.window.sheetParent endSheet:self.window returnCode:NSModalResponseCancel];
}

@end
