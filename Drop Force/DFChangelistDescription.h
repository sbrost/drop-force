//
//  DFChangelistDescription.h
//  Drop Force
//
//  Created by Steve Brost on 11/23/16.
//  Copyright © 2016 Software Pot Pie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DFChangelistDescription : NSObject
{

@private
    __strong NSDictionary* _dictionaryDesc;
}

-(instancetype)initWithDictionaryResponce:(NSDictionary*)dictionayRepsonce;

@property (readonly) NSString* identifier;
@property (readonly) NSString* type;
@property (readonly) NSString* client;
@property (readonly) NSString* changeDescription;
@property (readonly) NSString* function;
@property (readonly) NSString* status;
@property (readonly) NSString* time;
@property (readonly) NSString* user;

@end

