//
//  P4ClientApi+Singleton.h
//  Drop Force
//
//  Created by Steve Brost on 11/22/16.
//  Copyright © 2016 Software Pot Pie. All rights reserved.
//

#import "P4ClientApi.h"

@interface P4ClientApi (Singleton)

+ (P4ClientApi*)createNewClient;
- (void)showConnectionError:(NSError*)error;

- (void)runCommandInBlock:(NSString *)command
            withArguments:(NSArray *)args
                 delegate:(id<P4ClientApiCommandDelegate>)delegate;

+ (void)runCommandAsync:(NSString *)command
            withArguments:(NSArray *)args
                 delegate:(id<P4ClientApiCommandDelegate>)delegate;

@end


extern NSString* const kP4User;
extern NSString* const kP4Password;
extern NSString* const kP4Client;
extern NSString* const kP4Host;
extern NSString* const kP4PORT;
