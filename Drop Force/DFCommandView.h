//
//  DFCommandView.h
//  Drop Force
//
//  Created by Steve Brost on 6/22/12.
//  Copyright (c) 2012-2016 Software Pot Pie. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface DFCommandView : NSView
{
}

@property (nonatomic, strong) NSImage*      image;
@property (nonatomic, strong) NSString*     title;
@property (nonatomic, strong) NSString*     command;
@property (nonatomic, strong) NSDictionary* titleAttributes;
@property (nonatomic, strong) NSColor*      backgroundColor;

@end
