//
//  DFNewChangelistEditorWindowController.h
//  Drop Force
//
//  Created by Steve Brost on 11/23/16.
//  Copyright © 2016 Software Pot Pie. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface DFNewChangelistEditorWindowController : NSWindowController

+ (instancetype)newChangelistEditor;

@property (assign) IBOutlet NSTextView* descriptionView;
@property (assign) IBOutlet NSTextField* descriptionLabel;

@end
