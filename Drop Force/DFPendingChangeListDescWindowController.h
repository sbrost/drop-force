//
//  DFPendingChangeListDescWindowController.h
//  Drop Force
//
//  Created by Steve Brost on 5/15/17.
//  Copyright © 2017 Software Pot Pie. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class DFChangelistDescription;
@interface DFPendingChangeListDescWindowController : NSWindowController
{
@private
    IBOutlet NSTableView* _pendingFilesTable;
    NSMutableArray<NSDictionary*>*       _pendingFiles;
}

- (void)resetForChangeList:(DFChangelistDescription*)changeListDesc;
- (void)addPendingFile:(NSDictionary*)depotAndClientFile;

@property (readonly) NSArray* pendingFiles;

@end
