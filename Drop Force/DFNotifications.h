//
//  DFNotifications.h
//  Drop Force
//
//  Created by Steve Brost on 5/18/17.
//  Copyright © 2017 Software Pot Pie. All rights reserved.
//

#import <Foundation/Foundation.h>

//extern NSString* const kFileEditedNotification;
//extern NSString* const kFileAddedNotification;
//extern NSString* const kFileRevertedNotification;
//extern NSString* const kFileDeletedNotification;

extern NSString* const kFileActionNotification;
