//
//  DFPendingChangeListDescWindowController.m
//  Drop Force
//
//  Created by Steve Brost on 5/15/17.
//  Copyright © 2017 Software Pot Pie. All rights reserved.
//

#import "DFPendingChangeListDescWindowController.h"
#import "DFChangelistDescription.h"
#import "DFConstants.h"
#import "P4ClientApi+Singleton.h"


@interface DFPendingChangeListDescWindowController (Private)<NSTableViewDataSource, NSTableViewDelegate, P4ClientApiCommandDelegate>

-(void)doubleClickFile:(id)sender;

@end

@implementation DFPendingChangeListDescWindowController

- (instancetype)init
{
    self = [super initWithWindowNibName:@"DFPendingChangeListDescWindowController"];
    if (self)
    {
        _pendingFiles = [NSMutableArray new];
    }
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    _pendingFilesTable.target = self;
    [_pendingFilesTable setDoubleAction: @selector(doubleClickFile:)];
}

- (void)resetForChangeList:(DFChangelistDescription*)changeListDesc
{
    NSString* changelistName = [NSString stringWithFormat:@"%@ %@ %@",
                                changeListDesc.identifier == nil ? @"Default" : changeListDesc.identifier,
                                changeListDesc.changeDescription == nil || changeListDesc.changeDescription.length == 0 ? @"" : @"-",
                                changeListDesc.changeDescription == nil ? @"" : changeListDesc.changeDescription];
    
    self.window.title = changelistName;
    [_pendingFiles removeAllObjects];
    [_pendingFilesTable reloadData];
}

- (void)addPendingFile:(NSDictionary*)pendingFile
{
    [_pendingFiles addObject:pendingFile];
    [_pendingFilesTable reloadData]; //be more specific later
}


- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    return _pendingFiles.count;
}

/* This method is required for the "Cell Based" TableView, and is optional for the "View Based" TableView. If implemented in the latter case, the value will be set to the view at a given row/column if the view responds to -setObjectValue: (such as NSControl and NSTableCellView).
 */
- (nullable id)tableView:(NSTableView *)tableView objectValueForTableColumn:(nullable NSTableColumn *)tableColumn row:(NSInteger)row
{
    NSString* value = _pendingFiles[row][kDepotFileKey];
    return value;
    
}

-(void)doubleClickFile:(id)sender
{
    P4ClientApi* commandClient = [P4ClientApi createNewClient];
    [commandClient runCommand:@"info" withArguments:nil delegate:self];
    
    NSMutableString* clientFile = [NSMutableString stringWithString:_pendingFiles[_pendingFilesTable.clickedRow][kClientFileKey]];
    NSString* clientRoot = commandClient.userInfo; //we need the client root path, to create the full file path from the client file path. This is being set in the userInfo by the command didReceiveTaggedRepsonce
    NSLog(@"%@ \n%@", clientFile, clientRoot);
    
    NSString* clientName = [NSString stringWithFormat:@"//%@", commandClient.client];
    [clientFile replaceOccurrencesOfString:clientName withString:clientRoot options:NSLiteralSearch range:NSMakeRange(0, clientFile.length)];
    
    NSURL* fileURL = [NSURL fileURLWithPath:clientFile];
    [[NSWorkspace sharedWorkspace] openURL: fileURL];
}

-(void)clientApi:(P4ClientApi*)api didReceiveTaggedResponse:(NSDictionary*)response;
{
    api.userInfo = response[kClientRootKey];
}

@end
